package ru.neoflex.quizbot;

import lombok.extern.log4j.Log4j2;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.neoflex.quizbot.entity.Document;
import ru.neoflex.quizbot.entity.Interviewee;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import static ru.neoflex.quizbot.Main.getProps;

@Log4j2
public class QuizBot extends TelegramLongPollingBot {

    private static final String FULL_NAME_PATTERN = "[а-яА-Я\\D]+";
    private static final String ANSWER_PATTERN = "^[1-4]";
    private static final String BOT_USERNAME;
    private static final String BOT_TOKEN;
    private static final Properties PROPS;
    private static final String PDPP_FILE_PATH = "src/main/resources/Согласие_на_обработку_Персональных_Данных.docx";

    private Document doc = new Document();
    private List<List<Object>> questions;
    private List<Object> iDs;
    private List<List<Object>> answers;
    private HashMap<String, Interviewee> interviewees = new HashMap<>();

    protected QuizBot(DefaultBotOptions botOptions) {
        super(botOptions);
    }

    static {
        PROPS = getProps();
        BOT_USERNAME = PROPS.getProperty("bot.username");
        BOT_TOKEN = PROPS.getProperty("bot.token");
    }

    @Override
    public void onUpdateReceived(Update update) {
        log.debug("Update received {}", update.toString());
        String userId = update.getMessage().getFrom().getId().toString();
        String messageText = update.getMessage().getText();
        String chatId = update.getMessage().getChatId().toString();
        log.debug("User ID is {}, Message text is {}, Chat ID is {}", userId, messageText, chatId);
        if (questions == null) {
            initDoc();
        }
        if (userInterviewed(userId)) {
            sendMsg(chatId, "Вы уже проходили опрос!");
            return;
        }
        if (!interviewees.containsKey(userId) && !userInterviewed(userId)) {
            addRunningUser(update, userId);
            sendPersonalDataProcessingPolicy(chatId);
            sendMsg(chatId,
                    "Привет! Перед прохождением опроса, пожалуйста, прочитайте политику обработки персональных " +
                    "данных. Продолжая, вы соглашаетесь с Политикой. Для начала опроса введите свои имя и фамилию!");
            return;
        }
        Interviewee interviewee = interviewees.get(userId);
        log.info("Interviewee is {}", interviewee);
        if (messageMatchesFullNamePatternAndIntervieweeStarted(interviewee, messageText)) {
            sendMsg(chatId, "Ответом на вопрос может быть только цифра от 1 до 4!");
            return;
        } else if (messageMatchesFullNamePatternAndIntervieweeNotStarted(interviewee, messageText)) {
            setFullNameAndStarted(interviewee, messageText);
        }
        if (messageMatchesAnswerPatternAndIntervieweeNotStarted(interviewee, messageText)) {
            sendMsg(chatId, "Вначале введите имя и фамилию!");
            return;
        }
        if (messageMatchesAnswerPatternAndCorrect(interviewee, messageText)) {
            sendMsg(chatId, "Правильный ответ!");
            interviewee.incrementOffset();
            interviewee.incrementCorrectAnswers();
        } else if (messageMatchesAnswerPatternAndIncorrect(interviewee, messageText)) {
            sendMsg(chatId, "Неверный ответ!");
            interviewee.incrementOffset();
        }
        if (questionsRemained(interviewee)) {
            String question = prepareQuestion(interviewee);
            sendMsg(chatId, question);
        } else {
            sendMsg(chatId, "Опрос завершен!");
            doc.appendInterviewee(interviewee.serialize());
            initDoc();
            interviewees.remove(userId);
        }
    }

    private synchronized void setButtons(SendMessage sendMessage) {
        // Создаем клавиуатуру
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        // Создаем список строк клавиатуры
        List<KeyboardRow> keyboard = new ArrayList<>();

        // Первая строчка клавиатуры
        KeyboardRow keyboardFirstRow = new KeyboardRow();
        // Добавляем кнопки в первую строчку клавиатуры
        keyboardFirstRow.add(new KeyboardButton("1"));
        keyboardFirstRow.add(new KeyboardButton("2"));

        // Вторая строчка клавиатуры
        KeyboardRow keyboardSecondRow = new KeyboardRow();
        // Добавляем кнопки во вторую строчку клавиатуры
        keyboardSecondRow.add(new KeyboardButton("3"));
        keyboardSecondRow.add(new KeyboardButton("4"));

        // Добавляем все строчки клавиатуры в список
        keyboard.add(keyboardFirstRow);
        keyboard.add(keyboardSecondRow);
        // и устанваливаем этот список нашей клавиатуре
        replyKeyboardMarkup.setKeyboard(keyboard);
    }

    private synchronized void sendMsg(String chatId, String messageText) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(chatId);
        sendMessage.setText(messageText);
        setButtons(sendMessage);
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            log.error("Cannot send a message! \n {}", e.toString());
        }
    }

    private synchronized void sendPersonalDataProcessingPolicy(String chatId) {
        SendDocument sendDocument = new SendDocument();
        sendDocument.setChatId(chatId);
        sendDocument.setDocument(new File(PDPP_FILE_PATH));
        try {
            execute(sendDocument);
        } catch (TelegramApiException e) {
            log.error("Cannot send the document! \n {}", e.toString());
        }
    }

    private boolean questionsRemained(Interviewee interviewee) {
        return interviewee.getOffset() != questions.size() && interviewee.getOffset() <= questions.size();
    }

    private String prepareQuestion(Interviewee interviewee) {
        StringBuilder question = new StringBuilder();
        for (Object var : questions.get(interviewee.getOffset())) {
            question.append(var).append("\n");
        }
        return question.toString();
    }

    private void setFullNameAndStarted(Interviewee interviewee, String messageText) {
        String[] fullName = messageText.split(" ");
        interviewee.setFirstName(fullName[0]);
        interviewee.setLastName(fullName[1]);
        interviewee.setStarted(true);
        log.debug("Interviewee first name is {}, last name is {}, started: {}", fullName[0], fullName[1], interviewee.hasStarted());
    }

    private boolean validateAnswer(String answer, Interviewee interviewee) {
        return answer.equals(answers.get(0).get(interviewee.getOffset()).toString());
    }

    private void initDoc() {
        log.debug("Getting doc info...");
        doc.init();
        this.questions = doc.getQuestions();
        this.answers = doc.getAnswers();
        this.iDs = doc.getIDs().get(0);
        log.debug("Doc info retrieved successfully.");
    }

    private boolean userInterviewed(String userId) {
        return iDs.contains(userId);
    }

    private boolean messageMatchesFullNamePatternAndIntervieweeStarted(Interviewee interviewee, String messageText) {
        return messageText.matches(FULL_NAME_PATTERN) && interviewee.hasStarted();
    }

    private boolean messageMatchesFullNamePatternAndIntervieweeNotStarted(Interviewee interviewee, String messageText) {
        return messageText.matches(FULL_NAME_PATTERN) && !interviewee.hasStarted();
    }

    private boolean messageMatchesAnswerPatternAndIntervieweeNotStarted(Interviewee interviewee, String messageText) {
        return messageText.matches(ANSWER_PATTERN) && !interviewee.hasStarted();
    }

    private boolean messageMatchesAnswerPatternAndCorrect(Interviewee interviewee, String messageText) {
        return messageText.matches(ANSWER_PATTERN) && validateAnswer(messageText, interviewee);
    }

    private boolean messageMatchesAnswerPatternAndIncorrect(Interviewee interviewee, String messageText) {
        return messageText.matches(ANSWER_PATTERN) && !validateAnswer(messageText, interviewee);
    }

    private synchronized void addRunningUser(Update update, String userId) {
        interviewees.put(userId, new Interviewee(update.getMessage().getFrom().getId(),
                                                 update.getMessage().getFrom().getFirstName(),
                                                 update.getMessage().getFrom().getBot(),
                                                 update.getMessage().getFrom().getLastName(),
                                                 update.getMessage().getFrom().getUserName(),
                                                 update.getMessage().getFrom().getLanguageCode()));
        log.debug("New user registered.");
    }

    @Override
    public String getBotUsername() {
        return BOT_USERNAME;
    }

    @Override
    public String getBotToken() {
        return BOT_TOKEN;
    }
}
