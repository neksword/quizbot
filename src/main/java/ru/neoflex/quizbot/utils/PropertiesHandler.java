package ru.neoflex.quizbot.utils;

import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

@Log4j2
public class PropertiesHandler {

    private PropertiesHandler() {
        throw new IllegalStateException("Utility class!");
    }

    public static Properties initProps(String filePath) {
        Properties properties = new Properties();
        try (FileReader tableConfig = new FileReader(new File("src/main/resources/spreadsheet.properties"))) {
            properties.load(tableConfig);
            log.debug("Default properties loaded");
        } catch (IOException e) {
            log.warn(
                "Default properties file is not found or cannot be read. No defaults would be set. Consider " +
                "providing" + " all of the options in external config file. \n {} ",
                e.toString());
        }

        try (FileReader config = new FileReader(new File(filePath))) {
            properties.load(config);
            log.debug("Properties initialized");
        } catch (IOException e) {
            log.fatal(
                "Cannot find config file! Make sure config.file.path property is set and path is correct. Aborting. " + "\n {}",
                e.toString());
        }
        System.setProperty("http.proxyUser", properties.getProperty("proxy.user"));
        System.setProperty("http.proxyPassword", properties.getProperty("proxy.password"));
        System.setProperty("jdk.http.auth.tunneling.disabledSchemes", "");
        return properties;
    }

}
