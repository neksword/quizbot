package ru.neoflex.quizbot.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;
import org.telegram.telegrambots.meta.api.objects.User;

import java.util.ArrayList;
import java.util.List;

@Log4j2
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class Interviewee extends User {

    @Getter
    @Setter
    private String lastName;
    @Getter
    @Setter
    private String firstName;
    @Setter
    private boolean started = false;
    @Getter
    private Integer offset = 0;
    private Integer correctAnswers = 0;

    public Interviewee(Integer id, String firstName, Boolean isBot, String lastName, String userName,
                       String languageCode) {
        super(id, firstName, isBot, lastName, userName, languageCode);
    }

    public void incrementOffset() {
        this.offset++;
    }

    public void incrementCorrectAnswers() {
        this.correctAnswers++;
    }

    public boolean hasStarted() {
        return started;
    }

    public List<List<Object>> serialize() {
        List<List<Object>> row = new ArrayList<>();
        ArrayList<Object> cells = new ArrayList<>();
        cells.add(getId());
        cells.add(firstName);
        cells.add(lastName);
        cells.add(correctAnswers);
        row.add(cells);
        return row;
    }
}
