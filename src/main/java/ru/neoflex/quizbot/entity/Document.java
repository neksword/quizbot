package ru.neoflex.quizbot.entity;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.AppendValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.io.*;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import static ru.neoflex.quizbot.Main.getProps;

@Log4j2
@Getter
public class Document {

    private List<List<Object>> questions;
    private List<List<Object>> IDs;
    private List<List<Object>> answers;

    private static final String APPLICATION_NAME;
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";
    private static final String SPREADSHEET_ID;
    private static final String QUESTIONS_RANGE;
    private static final String ANSWERS_RANGE;
    private static final String IDS_RANGE;
    private static final String INTERVIEWEE_RANGE;
    private static final List<String> SCOPES = Collections.singletonList(SheetsScopes.SPREADSHEETS);
    private static final String CREDENTIALS_FILE_PATH;
    private static final Properties PROPS;

    static {
        PROPS = getProps();
        APPLICATION_NAME = PROPS.getProperty("application.name");
        SPREADSHEET_ID = PROPS.getProperty("spreadsheet.id");
        QUESTIONS_RANGE = PROPS.getProperty("questions.range");
        ANSWERS_RANGE = PROPS.getProperty("answers.range");
        IDS_RANGE = PROPS.getProperty("ids.range");
        INTERVIEWEE_RANGE = PROPS.getProperty("interviewee.range");
        CREDENTIALS_FILE_PATH = PROPS.getProperty("credentials.file.path");
    }

    /**
     * Creates an authorized Credential object.
     *
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) {
        try (FileInputStream in = new FileInputStream(new File(CREDENTIALS_FILE_PATH))) {
            GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));
            GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
                                                                                       clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline").build();
            LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
            return new AuthorizationCodeInstalledApp(flow, receiver)
                .authorize(PROPS.getProperty("google.spreadsheets.user.id"));
        } catch (IOException e) {
            log.fatal("Cannot find credentials.json file or google.spreadsheet.user.id not set or invalid! \n {}",
                      e.toString());
            throw new RuntimeException();
        }
    }

    private Sheets createSheetsService() {
        try {
            final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            return new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME).build();
        } catch (IOException | GeneralSecurityException e) {
            log.fatal("Secure connection cannot be established! Aborting. \n {}", e.toString());
            throw new RuntimeException();
        }
    }

    private List<List<Object>> getInfo(final String range, String majorDimension) {
        Sheets service = createSheetsService();
        try {
            return service.spreadsheets().values().get(SPREADSHEET_ID, range).setMajorDimension(majorDimension)
                          .execute().getValues();
        } catch (IOException e) {
            log.fatal("Spreadsheet id is not set or invalid! Aborting. \n{}", e.toString());
            throw new RuntimeException();
        }
    }

    public void init() {
        this.questions = getInfo(QUESTIONS_RANGE, "ROWS");
        this.IDs = getInfo(IDS_RANGE, "COLUMNS");
        this.answers = getInfo(ANSWERS_RANGE, "COLUMNS");
    }

    public void appendInterviewee(List<List<Object>> valueRange) {
        String valueInputOption = "RAW";
        String insertDataOption = "INSERT_ROWS";
        ValueRange requestBody = new ValueRange().setValues(valueRange);
        log.debug("Interviewee info to be written: {}", requestBody.toString());
        try {
            Sheets sheetsService = createSheetsService();
            Sheets.Spreadsheets.Values.Append request = sheetsService.spreadsheets().values()
                                                                     .append(SPREADSHEET_ID, INTERVIEWEE_RANGE,
                                                                             requestBody);
            request.setValueInputOption(valueInputOption);
            request.setInsertDataOption(insertDataOption);
            log.debug("Request: {}", request.toString());
            AppendValuesResponse response = request.execute();
            log.debug("Request is successful.");
            log.debug("Response received. Info written: {}", response.toPrettyString());
        } catch (IOException e) {
            log.fatal(
                "Cannot append values to spreadsheet! No values would be written. Check if the interviewee.range option is set properly! \n {} \n"
                + " {} \n {} \n {}",
                SPREADSHEET_ID, INTERVIEWEE_RANGE, requestBody.toString(), e.toString());
            throw new RuntimeException();
        }
    }
}