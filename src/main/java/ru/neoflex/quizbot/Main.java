package ru.neoflex.quizbot;

import lombok.extern.log4j.Log4j2;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.meta.ApiContext;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.Properties;

import static ru.neoflex.quizbot.utils.PropertiesHandler.initProps;


@Log4j2
public class Main {

    private static Properties properties = new Properties();

    public static Properties getProps() {
        return properties;
    }

    public static void main(String[] args) {

        if (args.length == 0 || !args[0].contains("--config=")) {
            log.fatal("Provide the bot with config file using \"--config\" option!");
            return;
        }

        properties = initProps(args[0].substring(9));
        try {
            Authenticator.setDefault(new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(properties.getProperty("proxy.user"),
                                                      properties.getProperty("proxy.password").toCharArray());
                }
            });

            ApiContextInitializer.init();
            DefaultBotOptions botOptions = ApiContext.getInstance(DefaultBotOptions.class);
            botOptions.setProxyHost(properties.getProperty("proxy.host"));
            botOptions.setProxyPort(Integer.parseInt(properties.getProperty("proxy.port")));
            botOptions.setProxyType(DefaultBotOptions.ProxyType.HTTP);
            TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
            telegramBotsApi.registerBot(new QuizBot(botOptions));
            log.debug("Bot is registered.");
        } catch (TelegramApiException e) {
            log.fatal("The Bot could not be registered! Check proxy settings. Aborting. \n {}", e.toString());
        }
    }
}
