﻿
# Telegram Quiz Bot
This is the bot that asks questions and puts the results into Google Spreadsheet.

To make it work, first of all you are to create a table in [Google Sheets](http://spreadsheets.google.com/).
Then go to [Google API Console](https://console.developers.google.com/start), create an OAuth client ID, download
it and save somewhere on your hard drive.

By default bot is configured to inspect the 2 sheets: `Статистика` and `Вопросы` for interviewees stats and questions respectively marked up as follows:

`Статистика`

 A  | B          | C         | D                  
----|------------|-----------|--------------------
 ID | First Name | Last Name | N. correct answers 

`Вопросы`

A       |B    |C     |D     |E     |F  |G
--------|-----|------|------|------|---|----
Question|Var.1|Var. 2|Var. 3|Var. 4|   |Answer
    
You would hardly ever need to change this, but if you need just override defaults of these options in your `properties` file:

- `questions.range` 
- `answers.range`
- `ids.range`
- `interviewee.range`

*Please note that stats and questions ranges are read as rows, answers and IDs ranges processed as columns.*

(In order to properly set the range see the [A1 notation reference](https://developers.google.com/sheets/api/guides/concepts#a1_notation).) 

## Config Reference
When you are done with the previous step you are ready to start the bot. Do not forget to provide the config file with `--config` option.

The complete list of options is listed below:

- `application.name` - name of your application in Google API Console. `default: NeoQuiz`
- `questions.range` - range in spreadsheet to read questions from. `default: Вопросы!A:E`
- `answers.range` - range in spreadsheet to read answers from. `default: Вопросы!G:G`
- `ids.range` - range in spreadsheet to read IDs from. `default: Статистика!A:A`
- `interviewee.` - range in spreadsheet to write interviewed users to. `default: Статистика`
- `bot.username` `default: neoflexquizbot`
- `bot.token`
- `spreadsheet.id`
- `proxy.host`
- `proxy.port`
- `proxy.user`
- `proxy.password`
- `credentials.file.path`
- `google.spreadsheets.user.id`

Options with no default value are mandatory and must be present in configuration file.